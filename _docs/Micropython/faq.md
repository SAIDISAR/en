---
title: Frequently Asked Questions
Description: Frequently asked questions to solve your problems

---
# Frequently Asked Questions

* **What to do when a NUCLEO-WB55 board is suddenly not detected by Windows?**

    1. Check the wiring.
    2. Try another USB port.
    3. Change the cable.
    4. Re-flash the board, see [“Linux or Windows Startup Guide”](index).

<br>

* **What to do when Windows declares the files as «corrupt file» and it is no longer possible to place a file on the board, to rewrite one or even to edit one?**

    This happens in rare cases when the board has not been used “cleanly”. Here are some tips:
    - Use Thonny, Mu or Geany (downloadable from the net).
    - Check that the correct COM port is detected: *Tool* -> *Option* -> *Interpreter* then go to *Micro Python* *Port COM xx*.
    - Open the file from the text editor. *File* -> *Open* then select “*This Computer*” and then use the browser to fetch the files from the board (*PYBFLASH:*).
    - Use Ctrl+D for the soft reboot, use the hard reset (reset button on the board) only in case of soft failure.
    - You have to re-flash the board.
    - If even the re-flash step fails, you must completely erase the microcontroller’s flash memory, with the STM32CubeProgrammer program that you can download from the STMicroelectronics <a href="https://www.st.com/content/st_com/en/products/development-tools/software-development-tools/stm32-software-development-tools/stm32-programmers/stm32cubeprog.html#get-software" target="_blank">here</a> (you will need to create a myST account) and then re-flash.
 
<br>
 
* **What to do when the sensors on the X-NUCLEO IKS01A3 Expansion Board only return “0’s”?**
    - Test the connections.
    - Avoid over-pressing the connectors.

<br>

* **What to do when nothing happens with the Grove shield?**
    - Check connections. Make sure that all pins are inserted into the Arduino connectors and that the Grove shield is not "offset" by one pin from the connectors.
    - Make sure the switch at the bottom left of the shield (below the A0 connector) is on 3.3V or 5V if the connected module requires this power supply voltage.

<br>

* **I connected the Grove RGB 16x2 LCD module to the Grove shield and it does not work. What do I do?**
    - Verify that the Grove shield power switch is properly positioned at 5V. </li>
    - Make sure that pull-up resistors are available for the I2C connector. This can be achieved by placing the X-NUCLEO IKS01A3 shield above the Grove shield (see next question).

<br>

* **How do I add pull-up resistors to the Grove Shield I2C connectors? How do I use the Grove shield and X-NUCLEO IKS01A3 expansion board simultaneously with the NUCLEO WB55?**
    - Preferably connect the Grove Shield to the NUCLEO-WB55 first and then the X-NUCLEO extension KS01A3 to the Grove Shield. As a result, access to 3 I2C connectors, the UART connector and 5 digital connectors is lost. The Grove Shield will therefore remain accessible: an I2C connector, digital connectors D4 and D8 and the 4 analog connectors A0, A1, A2, A3.
    - A NUCLEO-WB55 then X-NUCLEO IKS01A3 then Shield Grove stacking (in this order) is not mechanically compatible because of the ICSP connector. To do this you will need to either remove the ISP connector from the Grove Shield (it is not used by ST NUCLEO boards), or remove the 2 center jumpers from the X-NUCLEO IKS01A3 and replace them with weld bridges.

<br>

* **With the Grove LED module, which LED leg should I insert into the connector marked “+”, short or long?**
    - The long one! Be careful; reminder that LEDs are polarized. If you connect them incorrectly, you will probably destroy them.
