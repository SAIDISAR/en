---
title: Exercise with 8x7-segments display TM1638 with MicroPython
description: Exercise with 8x7-segments display TM1638 with MicroPython
---

# 8x7-segments Display TM1638

## Assembly

The connection of the display is set the following way :  
![tm1638-schema.png](images/tm1638-schema.png)
You can find the connection information on the back of the card.


| Afficheur         | ST Nucleo         |
| :-------------:   | :---------------: |
|      5V           |         5V        |
|      DIO          |         D4        |
|      CLK          |         D3        |
|      STB0         |         D2        |
|      GND          |         GND       |

## Usage

At first, it is necessary to include libraries :  

```python
# Libraries include
import tm1638
from machine import Pin
```

Next we initialise communication pins with the card :  

```python
# Card declaration
tm = tm1638.TM1638(stb=Pin('D2'), clk=Pin('D3'), dio=Pin('D4'))
```

Now the card is ready to be used and here are the instructions to use the different parts :  

- LEDs :  
`tm.leds(value)` : turn on/off leds depending on the value  
`tm.led(position, value)` : turn on/off led at selected position  

```python
# LEDs
tm.leds(0b01010101) # turn on 1 led every 2
tm.leds(0b00000001) # turn on the 1st led and turn off the others
tm.led(2, 0)	# turn off the 3rd led
tm.led(0,1) 	# turn on the 1st led
```

- Buttons :  
`tm.keys()` return a 8-bits word with 1 to indicate the button is pressed. e.g. if tm.keys() return 0b00100001, that means that the 1st and 6th buttons are pressed.  

```python
# Get information on the buttons
boutons = tm.keys()
```

- Segments :  
`tm.show('word')` : allow to write caracters 0-9, A-Z, space, -, ° (displayed using '*') and dots. Display is left aligned and doesn't erase what was previously written if the word is shorter than 8 caracters.  
`tm.number(number)` : allow to write an integer. Display is right aligned and erases non-filled cases.  
`tm.scroll('message', time)` : allow to write a message moving from right to left. "time" is optional and allow to change the scrolling speed.  

```python
# Segments
tm.show('  *--*  ')
tm.show('a.b.c.d.e.f.g.h.')

tm.number(-1234567)
tm.number(1234)

tm.scroll('Hello World')
tm.scroll('Hello World Hello World', 100)
```

- Brightness adjustment :  
It is possible to change the leds and segments brightness with `tm.brightness(0)` or to turn them off with `tm.clear()`.  

```python
# Modify leds and segments brightness to the minimum
tm.brightness(0)

# Turn off leds and segments
tm.clear()
```

[La Bibliothèque TM1638](https://github.com/mcauser/micropython-tm1638)
