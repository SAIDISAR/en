---
title: Resources
description: Resources

---
# Resources

## Works

 [MicroPython et Pyboard, Python sur microcontrôleur : de la prise en main à l'utilisation avancée (Editions ENI)](https://www.editions-eni.fr/livre/micropython-et-pyboard-python-sur-microcontroleur-de-la-prise-en-main-a-l-utilisation-avancee-9782409022906)

## Online Documentation

**Official Micropython site :**

 [https://micropython.org/](https://micropython.org/)

**General documentation on MicroPython :**

 [http://docs.micropython.org/en/latest/](http://docs.micropython.org/en/latest/)

**Example of using the pyb library; very useful, taking into account the pin differences between the NUCLEO-WB55 and the Pyboard:**
 
 [http://docs.micropython.org/en/latest/pyboard/quickref.html#general-board-control](http://docs.micropython.org/en/latest/pyboard/quickref .html#general-board-control)

**STMicroelectronics boards with MicroPyhton firmware :**
 
 [https://micropython.org/stm32/](https://micropython.org/stm32/)

**Source code of the MicroPython project :**

 [https://github.com/micropython/micropython](https://github.com/micropython/micropython)

**Official Pythin 3 site :**

 [https://www.python.org/](https://www.python.org/)

**Technical Documentation of the NUCLEO-WB55 :**

 [https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html](https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html)

**Documentation of the Android application STBLESensor :**

 [https://github.com/STMicroelectronics/STBlueMS_Android](https://github.com/STMicroelectronics/STBlueMS_Android)

**Ressurces for Python 3 for the embedded (drivers) :**
 - [Libraries and EPS8266 Drivers for MicroPython)](https://github.com/mchobby/esp8266-upy)
 - [A comprehensive summary of pilots and libraries for MicroPython](https://awesome-micropython.com/)
 - [Libraries and drivers for Adafruit CircuitPython (easily transposable to MicroPython)](https://github.com/adafruit/Adafruit_CircuitPython_Bundle/tree/master/libraries/drivers)
 - [Seeed-Studio grove.py libraries and drivers (for Raspberry Pi mainly)](https://github.com/Seeed-Studio/grove.py)
 - [Libraries and drivers for the OpenMV board (for MicroPython)](https://github.com/openmv/openmv/tree/master/scripts/libraries) 
