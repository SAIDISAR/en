---
title: Downloads
description: Links to download firmware and tutorials, links to resources on partner sites

---
# STM32python downloads and links

## Software Tools

 - [Notepad++](https://notepad-plus-plus.org/)
 - [TeraTerm](https://osdn.net/projects/ttssh2/)
 - [Thonny](https://thonny.org/)
 - [PuTTY](https://www.putty.org/)
 - [PyScripter](https://sourceforge.net/projects/pyscripter/)
 - [Mu](https://codewith.mu/) (with Pyboard mode)
 - [Geany](https://www.geany.org/)


## Firmware(s)

  - [Firmware 1.17](../../assets/../fr/Firmware/firmware_micropython1.17.bin)


## MicroPython scripts for tutorials on this site

- **Tutorials to start with NUCLEO-WB55**
You will find [here](../../assets/Script/NUCLEO_WB55.zip) a ZIP file that brings together all the source codes presented in the [Start with NUCLEO-WB55] (STARTWB55) section.

- **Tutorials with X-NUCLEO-IKS01A3**
You will find [here](../../assets/Script/X_NUCLEO_ISK01A3.zip) a ZIP file that brings together all the source codes presented in the section [Tutorials with the X-NUCLEO-IKS01A3 board](IKS01A3).

- **Tutorials with external modules**
You will find [here](../../assets/Script/MODULES.zip) a ZIP file that brings together all the source codes presented in the [Tutorials with external modules] (grove) section.

- **Tutorials with BLE**
You will find [here](../../assets../..fr/Script/BLE.zip) a ZIP file that will bring together all the source codes presented in the [Tutorials with BLE] (BLE) section.

## Other Resources

- [**Connected lamp**](http://icnisnlycee.free.fr/index.php/61-stm32/ble-en-micropython/78-exemple-ble-lampe-connectee)<br>
Remotely control a lamp via BLE with MIT App Inventor. **Thanks to Julien Launay! **<br>

- [**Weather Station**](../../assets/projects/station_meteo.zip)<br>
Prototyping a weather station inspired by the Ikea Klockis product. **Thanks to Christophe Priouzeau & Gérald Hallet! **<br>
