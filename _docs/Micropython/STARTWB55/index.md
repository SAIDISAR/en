---
title: Start with MicroPython and the NUCLEO-WB55
description: Start with MicroPython and the NUCLEO-WB55
---

# Start with MicroPython and the NUCLEO-WB55

This section provides some exercises to start programming in MicroPython with the NUCLEO-WB55 board. We will discuss and explain -in the form of tutorials- notions of embedded programming and microcontroller architecture that will be essential for you to progress. When you come across an acronym that you are not familiar with, don’t hesitate to consult the [glossary](../../Microcontrollers/glossaire).

## Video presentation of the board by the students of the Lycée Vilgénis in Massy

To get started, we invite you to watch this excellent Youtube video presenting the board:

<br>

<iframe width="800" height="600" src="https://www.youtube.com/embed/882OQxTISWI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<br>

**We thank Mr. Jacques Taillet, teacher at the Lycée Vilgénis in Massy (Versaille Academy) and the students of his scientific club for the production and sharing of this document!**


## List of tutorials 

The following links give some examples of the implementation of the NUCLEO-WB55 card. This is a reminder that all the scripts presented here or used later can be retrieved from the [**page Downloads**](../Telechargement) in addition to the links that may be present throughout the text.

- [Calling Arduino Pins](arduino_pins)
- [Blink an LED](blink)
- [Program a LED chaser](chaser)
- [Blink multiple LEDs simultaneously](blink_many)
- [Program one or multiple buttons](button)
- [Manage buttons with interrupts](button_it)
- [Converting an analog value](adc)
- [Implementation of the I2C bus with an OLED display](oled)
- [Implementation of PWM with a buzzer](../grove/buzzer)
- [Use serial communication](uart)
- [Use the Real-Time Clock (RTC) built into the STM32WB55](rtc)
- [Use the Independent Watchdog (IDWG) built into the STM322WB55](watchdog)
- [Use Bluetooth Low Energy](../BLE/index)

