---
title: Blink multiple LEDs simultaneously
description: How to create a multi-tasking application with MicroPython
---

# Blink multiple LEDs

This tutorial addresses the topic of multitasking programming with MicroPython. Of course, we are not going to build an operating system!
First, we will limit ourselves to demonstrating how, through clever time management, and possibly the use of *timers*, it is possible to give the illusion that the STM32WB55 performs several **independent** tasks simultaneously. We chose three very simple tasks using the LEDs integrated in the NUCLEO-WB55:

 - Task 1: Flashes the red LED (screen printed *LED3* on the PCB) at 0.5 Hz
 - Task 2: Flashes the green LED (screen printed *LED2* on the PCB) at 2 Hz
 - Task 3: Flashes the blue LED (screen printed *LED1* on the PCB) at 10 Hz

But MicroPython is a very powerful API that also allows you to simply **implement true multitasking** with the [`uasyncio`](https://docs.micropython.org/en/latest/library/uasyncio.html) and [`thread`](https://docs.micropython.org/en/latest/library/_thread.html) libraries. We will finish our tutorial with a script implementing the library `uasyncio` (knowing that `thread `, which is easier to use, is still in the beta phase at the time this tutorial is written). 

## Method number 1: Time sharing with time.ticks_ms()

> **The following scripts are available in [the download area](https://stm32python.gitlab.io/fr/assets/Script/MODULES.zip)**.

This first approach is directly inspired by many examples found on the Internet, usually for the Arduino API.
All processing is done entirely in the "main loop" (*while True:*).

### The *main.py* code

```python
import time # Library to manage timeouts

# Variable to track the time elapsed since the start of the script
n = 0

# Global variables for LED flashing dates
blink1 = 0
blink2 = 0
blink3 = 0

while True: # Infinite loop

	# Test the elapsed time for each task.
	# Note that the duration tests are not ran on a (strict) equality but on an exceedance.
	# For example, we write "if time.ticks_diff(now, blink1) > 99" and not "if time.ticks_diff(now, blink1) == 99".
	# Indeed, it is unlikely that the infinite loop will perform its test at the exact planned moment.
	# So there will be some (low) uncertainty about the flashing frequencies of the LEDs.
	
	# Number of milliseconds elapsed since script launch
	n = time.ticks_ms()
	
	# Task 1: Blinking the red LED
	if time.ticks_diff(n, blink1) > 1999: # Every 2000 ms since the last inversion...
		pyb.LED(1).toggle() # Inverting the LED...
		blink1 = n # Memorizing the date of that event.
	
	# Task 1: Blinking the green LED
	if tim2.ticks_diff(n, blink2) > 499: # Every 500 ms since the last inversion...
		pyb.LED(2).toggle() # Inverting the LED...
		blink2 = n # Memorizing the date of that event.
	
	# Tâche 3 : clignotement de la LED bleue
	if time.ticks_diff(n, blink3) > 99: # Every 100 ms since the last inversion...
		pyb.LED(3).toggle() # Inverting the LED...
		blink3 = n # Memorizing the date of that event.
```

The code is particularly readable, but the method is ruthless for the microcontroller which is active and consumes energy 100% of the time while the blinking of the LEDs - the useful part of the program - occupies only a very small fraction of the time. This method can easily be generalized to more "tasks" (flash other LEDs, scan several buttons, etc.). But another of its limitations appears immediately: the readability and the ease to maintain and modify the program will quickly deteriorate when we multiply the "tasks".


## Method Number 2: Time Sharing Using a System Timer

> **The following scripts are available in [the download area](https://stm32python.gitlab.io/fr/assets/Script/MODULES.zip)**.

This second approach uses a *timer* that is started before the main loop and counts the time independently. You can imagine the timer as a counter built into the microcontroller.

Once configured and started, a timer will increment its internal CNT counter in steps of 1, from 0 to a maximum value, for example CNT_max = 4095. When CNT = CNT_max the timer generates a *interrupt* called "*counter overflow*". At this point, CNT returns to zero and the timer restarts its counting cycle: CNT = 0, CNT = 1... The following diagram shows the counting of a timer for CNT_max=5.

<br>
<div align="center">
<img alt="Principe décompte timer" src="images/timer.jpg" width="800px">
</div>
<br>

We use the counter overflow interrupt to generate a time base that is used for blinking LEDs.
This version isn't much more complicated compared to the first solution and it has an advantage: it is no longer the Cortex M4 that counts the elapsed time in the main loop, but one of the timers of the STM32WB55. The Cortex M4 will therefore be relieved of this task and we can imagine that the timing of the diodes will be more precise. It remains 100% (of the time) monopolized by the main loop but, this time, because we use an interruption, we can use the *pyb.wfi()* function to place the microcontroller in *energy saving mode until the next interruption* (which will "wake him up").

### *main.py* code

```python
from pyb import Timer, wfi # To manage timers and energy saving mode

# Global variable that will serve as a time replay
n = 0

# Global variables for LED flashing dates
blink1 = 0
blink2 = 0
blink3 = 0

# Interrupt Service Routine (ISR) of the Timer 1 Counter Overflow.
# It increments variable n by 1 every 100-ths of a second.
def tick(timer):
	global n # VERY IMPORTANT, do not forget the keyword "global" in front of the variable n
	n += 1

# Starts timer 1 at the frequency of 100 Hz.
tim1 = Timer(1, freq=100)

# Assigns the "tick" function to the timer 1 overflow interrupt.
# It will be called 100 times per second.
tim1.callback(tick)

while True: # Infinie loop

	# Time is tested; each unit of n indicates that one hundredth of a second has elapsed.
	# Note that the duration tests are not about a (strict) equality but about an exceedance.
	# For example, we write "n - blink1 > 9" and not "n - blink1 == 10".
	# Because the timer increments n ideatively, it is likely that the infinite loop "misses" the value
	# n = 10 the essential of its iterations.
	
	wfi() # Puts the microcontroller in energy-saving mode
	
	if n - blink1 > 9: # Every 0.1s since its last inversion...
		pyb.LED(3).toggle() # Inverting the blue LED
		blink1 = n # Memorizing the date of the event
	if n - blink2 > 49: # Every 0.5s since its last inversion...
		pyb.LED(2).toggle() # Inverting the green LED
		blink2 = n # Memorizing the date of the event
	if n - blink3 > 199: # Every 2s since its last inversion...
		pyb.LED(1).toggle() # Inverting the red LED
		blink3 = n # Memorizing the date of the event
		
		# Time counters are reset when the LED with the longer period has changed its state.
		# This is to avoid an overflow of the variable n if the script "runs" too long.
		n = 0
		blink1 = 0
		blink2 = 0
		blink3 = 0
```

## Method number 3: Use multiple timers

> **The following scripts are available in the [the download area](https://stm32python.gitlab.io/fr/assets/Script/MODULES.zip)**.

This third approach uses three timers of the STM32WB55, set them to the desired frequencies and uses their respective counter overflow interrupts to invert the LEDs.

- **Advantage 1**: We are getting closer to a real hardware multitasking, but we can’t quite do it because the service functions of the timers overflow interrupt must be executed by the Cortex M4 of the STM32WB55.
- **Advantage 2**: There is no "main" program (ie: "while True:") so this approach consumes much less energy than the previous ones.
- **Drawback**: We use a timer to drive each LED. This debauchery of hardware resources will quickly reach its limits. The MicroPython interpreter for the NUCLEO-WB55 implementing 4 timers (TIM1, TIM2, TIM16 and TIM17) will not be able to handle more than 4 LEDs (or, more generally, 4 tasks) in this way.

### Variant 1: The code with *lambda-expressions*

We use *lambda-expressions* to respond to timers interruptions. It is a compact and formal writing specific to MicroPython.

```python
from pyb import Timer # Library to manage timers

tim1 = Timer(1, freq= 0.5) # Timer 1 frequency set to 0.5 Hz

# Callback: calls the service routine of the timer overflow interrupt.
# Here this routine is pyb.LED(1). toggle(), the writing is condensed into a lambda-expression.
tim1.callback(lambda t: pyb.LED(1).toggle()) # Inverting the red LED once every two seconds.

tim2 = Timer(2, freq= 2) # Timer 2 frequency set to 2 Hz
tim2.callback(lambda t: pyb.LED(2).toggle()) # Inverting the green LED twice every two seconds.

tim16 = Timer(16, freq=10) # Timer 16 frequency set to 10 Hz
tim16.callback(lambda t: pyb.LED(3).toggle()) # Inverting the red LED ten times every two seconds.
```

### Variant 2: Code without *lambda-expressions*

We explicitly use **service routines for the timers overflow's three ISR interruptions*, a writing equivalent to lambda-expressions but more conventional in embedded programming.

Optimizations are also implemented:

 - The decorator (i.e. the directive) `@micropython.native` before a function, which tells the bytecode compiler to generate a binary code specific to the STM32WB55. In general, this leads to a twice-efficient program at the cost of some detailed code writing constraints [here](https://docs.micropython.org/en/v1.9.3/pyboard/reference/speed_python.html#the-native-code-emitter).
 - A *while True:* loop that reduces microcontroller consumption due to the *pyb.wfi()* instruction already presented.

```python
from pyb import Timer # Library to manage timers

@micropython.native # Asks MicroPython to generate a binary code optimized for this function
def blink_LED_red(timer): # ISR of timer 1
	pyb.LED(1).toggle()

tim1 = Timer(1, freq= 0.5) # Timer 1 frequency set to 0.5 Hz
# Calls the ISR of the timer 1 overflow interrupt: the blink_LED_red function
tim1.callback(blink_LED_red)

@micropython.native
def blink_LED_green(timer): # ISR of timer 2
	pyb.LED(2).toggle()

tim2 = Timer(2, freq= 2) # Timer 2 frequency set to 2 Hz
# Calls the ISR of the timer 2 overflow interrupt: the blink_LED_green function
tim2.callback(blink_LED_green)

@micropython.native
def blink_LED_blue(timer): # ISR of timer 16
	pyb.LED(3).toggle()

tim16 = Timer(16, freq=10) # Timer 16 frequency set to 10 Hz
# Calls the ISR of the timer 16 overflow interrupt: the blink_LED_blue function
tim16.callback(blink_LED_blue)

@micropython.native
def main():
	while True:
		pyb.wfi() # Puts the micorocontroller in energy-saving mode

# main loop
main()
```

## Method 4: Multitasking with *uasyncio*

> **The following scripts are available in [the download area](https://stm32python.gitlab.io/fr/assets/Script/MODULES.zip)**.

The latter approach uses the [`uasynco`](https://docs.micropython.org/en/latest/library/uasyncio.html) library built into the MicroPython firmware. This is an excellent introduction to the notions and mechanisms of *real-time operating systems for embedded systems* (RTOS) such as [*FreeRTOS*](https://en.wikipedia.org/wiki/FreeRTOS) which is very used, but also very delicate to master. While it offers opportunities reminiscent of an RTOS, [`uasyncio`] is not as powerful because it does not manage the priority of tasks and does not guarantee the time required to complete them. The simplicity of its implementation and the frugality of its use of the RAM are bound to have some counterparts!

The example of asychrone programming that we will use is surprisingly easy to implement in MicroPython and can be easily transposed to many other applications for which the system must remain responsive to user interaction. You will find such an example with the [**Grove Gesture Sensor Module Tutorial PAJ7620U2**](../grove/paj7620u2).

It should be noted that this approach does not involve any interruptions and that it therefore makes maximum use of the microcontroller. It is elegant and easy to implement but it is not adapted to reduce energy consumption.

### *main.py* code

```python
# Object of the script: 
# Blinking three NUCLEO-WB55 LEDs simultaneously at different frequencies.
# The "uasyncio" library is used for asynchronous programming.
# Code directly adapted from https://docs.micropython.org/en/latest/library/uasyncio.html
# Tutorial on uasyncio: 
#  https://github.com/peterhinch/micropython-async/blob/master/v3/docs/TUTORIAL.md

import uasyncio # Import library for asynchronous execution
print("Version de uasyncio : ", uasyncio.__version__) # uasyncio version

# Asynchronous coroutine to blink the LED on the led pin
@micropython.native # Decorator to generate "native" STM32WB55 binary code (more efficient)
async def blink(led, period_ms):
	while True:
		# Turns on the LED
		led.on() 
		# Non-blocking Timeout of "periods_ms" milliseconds
		await uasyncio.sleep_ms(period_ms)
		# Turns off the LED
		led.off()
		# Non-blocking Timeout of "periods_ms" milliseconds
		await uasyncio.sleep_ms(period_ms)

# Creates three competing "blink" spots, one per LED
@micropython.native
async def main(led_rouge, led_verte, led_bleue):

	task = uasyncio.create_task(blink(led_rouge, 2000)) # Task for the red LED
	uasyncio.create_task(blink(led_verte, 500)) # Task for the green LED
	uasyncio.create_task(blink(led_bleue, 100)) # Task for the blue LED

	# Extends program execution until "task" is complete 
	# (which will never happen)
	await task

# Code in pyboard syntax (valid with NUCLEO-WB55)
from pyb import LED
# Calling the planner which initiates the execution of the main function with its three 
# concurrent tasks. No script instruction below this 
# line will be executed.
uasyncio.run(main(LED(1), LED(2), LED(3)))

# Code in generic syntax (for external LEDs for example, connected
# to pins D2, D3, D4).
# from machine import Pin
# uasyncio.run(main(Pin('D2'), Pin('D3'), Pin('D4')))
```

You will find **a complete tutorial** on the use of the `uasyncio` library on [this page](https://github.com/peterhinch/micropython-async/blob/master/v3/docs/TUTORIAL.md).
