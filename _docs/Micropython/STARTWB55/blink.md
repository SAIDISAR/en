---
title: Blink an LED
description: Tutorials for blinking an LED with MicroPython
---

# Blink an LED

This tutorial explains how to flash a *light emitting diode* with MicroPython.
This example is adapted from the Adafruit tutorial available [here](https://learn.adafruit.com/micropython-basics-blink-a-led/blink-led) which also details the REPL communication with the microcontroller.

## Required Tools

The NUCLEO-WB55 board. We will flash LED 1 (blue):

<div align="center">
<img alt="LEDS" src="images/leds.jpg" width="500px">
</div>

## MicroPython code

**The following scripts are available in the [download area](https://stm32python.gitlab.io/fr/assets/Script/NUCLEO_WB55.zip)**.

Edit the *main.py* script contained in the NUCLEO-WB55-associated USB disk directory: *PYBFLASH*.

``` python
# Script object:
# Example of flashing the blue NUCLEO-WB55 LED at a given frequency.

import pyb # for device access (GPIO, LED, etc.)
from time import sleep # for system breaks (among others)

# Blue LED Initialization
led_bleue = pyb.LED(3) # LED1 screen-printed on the PCB

duration = 0.5 # Waiting time before changing LED status

# The loop will repeat ten times (for i from 0 to 9)
for i in range(10):

	# Displays the iteration index on the USB User serial port
	# See https://www.geeksforgeeks.org/python-output-formatting/ for the explanation of the following line.

	# print("Itération {:2d}".format(i))

	# There are several ways to display a value with print, below is a more readable alternative

	print("Itération %d : "%i)

	led_bleue.on() # Turns on the LED
	print("LED bleue allumée")
	sleep(duration) # Waits for "duration" seconds

	led_bleue.off() # Turns off the LED
	print("LED bleue éteinte")
	sleep(duration) # Waits for "duration" seconds
```

You can start the script with Ctrl + D on the PuTTY terminal and observe the messages it sends while the LED flashes at a frequency of 1 Hz (one on - off cycle per second):

<div align="center">
<img alt="Sortie blink" src="images/output_blink.png" width="700px">
</div>

## To go further

The variants around this example are endless ... 
 - You will find on [this page](blink_many) its application in blinking three LEDs of the NUCLEO-WB55.
 - You will find [here](chaser) how to blink the three LEDs of the NUCLEO-WB55 in sequence.
