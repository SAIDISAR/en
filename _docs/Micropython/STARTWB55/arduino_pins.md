---
title: Calling Arduino pins in MicroPython scripts
description: Arduino pin calls by MicroPython scripts on the NUCLEO-WB55
---

# Calling Arduino pins in MicroPython scripts

The NUCLEO-WB55 is suitably equipped with Arduino pins, so the mapping (standardized by the Arduino UNO board) is recalled [here](../stm32wb55).

## Case 1: you are using the firmware distributed on this site

**In the MicroPython firmware available for download [here](../Downloads), Arduino pin aliases have been redefined so that their call is as simple as possible**. For example, [The exercise with the Grove buzzer](../grove/buzzer), **connected on the `D3` pin**, gives the following script *main.py*:

```python
# Object of the script: Play a jingle on a buzzer (Grove or other).
# # This example demonstrates the use of PWM for pin D3 on which 
# the buzzer is connected.

from pyb import Pin, Timer

# List of notes that will be played by the buzzer
frequency = [262, 294, 330, 349, 370, 392, 440, 494]

# D3 generates a PWM with TIM1, CH3
BUZZER = Pin('D3') 

while True :
	# Iteration between 0 and 7
	for i in range (0,7) :
		# Adjust frequency during iteration
		tim1 = Timer(1, freq=frequency[i])
		ch3 = tim1.channel(3, Timer.PWM, pin=BUZZER)
		# Duty Cycle set to 5% (the buzzer is powered 5% of the time of a period)
		ch3.pulse_width_percent(5)
```

The `D3` pin call **is simply done with the `Pin('D3')` instruction.**


## Case 2: you are using a firmware distributed on the official MicroPython website

By default, **the MicroPython firmware for boards equipped with STM32 microcontrollers does not use the same aliases as the Arduino API to access pins, instead it uses those of the Pyboard**. Also, **if you are using a MicroPython firmware from [the site micropython.org](https://micropython.org/stm32/)**, You may find it convenient to redefine aliases to Arduino pins using the procedure we will explain.

Please note that all the examples we give on this site use a modified firmware that already integrates aliases for Arduino pins and do not require (at this time) this redefinition of their names.

Start by saving the following content to a file named *arduino_pins.py*.

```python
# Equivalence between Arduino pins / Pyboard pins
from pyb import Pin
A0=Pin.cpu.C0
A1=Pin.cpu.C1
A2=Pin.cpu.A1
A3=Pin.cpu.A0
A4=Pin.cpu.A4
A5=Pin.cpu.A5
D0=Pin.cpu.A3
D1=Pin.cpu.A2
D2=Pin.cpu.C6
D3=Pin.cpu.A10
D4=Pin.cpu.C10
D5=Pin.cpu.A15
D6=Pin.cpu.A8
D7=Pin.cpu.C13
D8=Pin.cpu.C12
D9=Pin.cpu.A9
D10=Pin.cpu.A4
D11=Pin.cpu.A7
D12=Pin.cpu.A6
D13=Pin.cpu.A5
D14=Pin.cpu.B9
D15=Pin.cpu.B8
```

This file must be copied to *PYBFLASH* and imported into the *main.py* script of your application. For example, the modified version of [the exercise with the Grove buzzer](../grove/buzzer) will give the following *main.py* script:

```python
# Object of the script: Play a jingle on a buzzer (Grove or other).
# This example demonstrates the use of PWM for pin D3 on which
# the buzzer is connected.

import arduino_pins # line for accessing Arduino pins 
from pyb import Timer

# List of notes that will be played by the buzzer
frequency = [262, 294, 330, 349, 370, 392, 440, 494]

# D3 generates a PWM with TIM1, CH3
BUZZER = arduino_pins.D3 # Pin call according to the Arduino Convention

while True :
	# Iteration between 0 et 7
	for i in range (0,7) :
		# Adjust frequency during iteration
		tim1 = Timer(1, freq=frequency[i])
		ch3 = tim1.channel(3, Timer.PWM, pin=BUZZER)
		# Duty Cycle set to 5% (the buzzer is powered 5% of the time of a period)
		ch3.pulse_width_percent(5)
```

**All the examples we give on this site use the Arduino pin definitions given in the "Case 1"** and adapted to our "custom" firmware. Also, if you use a firmware of the MicroPython site, you will (possibly) have to make the changes we have just explained.
