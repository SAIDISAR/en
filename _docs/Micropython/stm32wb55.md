---
title: STM32WB55 development kit
description: STM32WB55 development kit

---
# STM32WB55 development kit
## Nucleo STM32WB55 board overview

The STM32-NUCLEO board is designed with the necessary components to start the microcontroller.

Extension connectors (ARDUINO, MORPHO) allow the user to connect external electronic components to the STM32.

The board has also a connector for a CR2032 battery, 3 buttons (SW1, SW2, SW3), 3 LEDs (red, green and blue) and 2 micro-USB connectors.

_Here is the block diagram of the NUCLEO-WB55 kit:_
 
![image](images/architecture.png)

## STM32WB55 Nucleo board description

_Development kit bottom view:_

![image](images/board.png)


### PCB Antenna

This is the bluetooth antenna of the STM32WB55 microcontroller. It is a so-called PCB antenna because it is integrated directly into the circuit of the board. Its complex shape is designed to optimize the reception and emission of radio waves for the bluetooth frequency of 2.4 GHz.


### Arduino / Morpho connectors 

Extension kits can be added simply, thanks to standardized connectors.

For example, you can connect the Mems Microphone kit (X-NUCLEO-CCA02M2) to the NUCLEO-WB55 board:

![image](images/boards.png)


### USB User

This is the USB port that will be used to:

* Communicate with the MicroPython interpreter.
* Program the system.
* Power the development kit


### Socket CR2032

Once the system has been programmed, it will be possible to supply the kit with a CR2032 battery, in order to make the system portable.


#### STLINK

This is the tool that allows you to program the microcontroller, for that, use the "drag and drop" to drop the firmware on the USB device: ** WB55-NODE **.

### User LEDs

These LEDs are accessible to the developer and can be activated using MicroPython.

See the example "change the state of an Output" (GPIO) (Turn on an LED)

