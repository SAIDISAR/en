---

title: Basic exercises with MicroPython
description: Basic exercises with MicroPython

---
**The source code for each exercise is available in the [download area](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement)**<br><br>

## Retrieve the state of a Button (GPIO) (Reading the state of a button)

There are 3 buttons SW1, SW2, SW3 available for the developer: 

![image](images/boutons.png)

We will see in this sub-section how to initialize a Pin in "Input" mode and display a message when pressing one of the 3 buttons using **pyb.Pin**. 

We will use the so-called “pulling” method to ask the MicroPython system for the state of the Pin (1 or 0). 

For electronic reasons, the state of the Pin at rest, button released, is equivalent to 1 while the state during a button press is 0.

*   Open the script editor and edit the *** main.py file***:
 
``` python
import pyb # MicroPython library allowing access to peripherals (GPIO, LED, etc.)
import time # Library allowing system pauses
print ("GPIOs with MicroPython are easy")
# Initialization of the input pins (SW1, SW2, SW3)
sw1 = pyb.Pin ('SW1', pyb.Pin.IN)
sw1.init (pyb.Pin.IN, pyb.Pin.PULL_UP, af = -1)
sw2 = pyb.Pin ('SW2', pyb.Pin.IN)
sw2.init (pyb.Pin.IN, pyb.Pin.PULL_UP, af = -1)
sw3 = pyb.Pin ('SW3', pyb.Pin.IN)
sw3.init (pyb.Pin.IN, pyb.Pin.PULL_UP, af = -1)
# Initialization of variables
old_sw1_value = 0
old_sw2_value = 0
old_sw3_value = 0
while 1: # Create an infinite loop
	# the System falls asleep for 300ms
	time.sleep_ms (300)
	# Recovery of the state of Buttons 1,2,3
	sw1_value = sw1.value ()
	sw2_value = sw2.value ()
	sw3_value = sw3.value ()
	# Is the current state different from the previous state?
	if sw1_value! = old sw1_value:
    		if sw1_value == 0:
        	print ("Button 1 (SW1) is pressed")
    else:
        	print ("Button 1 (SW1) is released")
    old_sw1_value = sw1_value
if sw2_value! = old_sw2_value:
    if sw2_value == 0:
        print ("Button 2 (SW2) is pressed")
    else:
        print ("Button 2 (SW2) is released")
    old_sw2_value = sw2_value
if sw3_value! = old sw3_value:
if sw3_value == 0:
        print ("Button 3 (SW3) is pressed")
    else:
        print ("Button 3 (SW3) is released")
    old_sw3_value = sw3_value
```

* Save the * main.py * script (CTRL + S), then restart the card (CTRL + D).

When you press one of the 3 buttons (SW1, SW2, SW3), messages in the console tell you the status of the buttons.

## Change the state of an Output (GPIO) (Turn on an LED)

The goal now is to light the LEDs on the development kit:

![image](images/leds.png)



Here is the organization of the LEDs by color and number:


1.LED 1: Blue
2.LED 2: Green
3.LED 3: Red


Under MicroPython, the ** pyb.LED ** module allows you to manage LEDs very simply.

In this part, we want to carry out a “chaser”, this exercise consists of turning on and off the LEDs one after the other, cyclically.

*   En utilisant la méthode précédente et le script suivant, réaliser le chenillard avec MicroPython:

``` python
import pyb
import time
print ("LEDs with MicroPython is easy")
# Initialization of LEDs (LED_1, LED_2, LED_3)
led_bleu = pyb.LED (1)
led_vert = pyb.LED (2)
led_rouge = pyb.LED (3)
# Initialization of the LED counter
led_counter = 0
while 1: # Create an infinite loop
    if led_counter == 0:
        led_bleu.on ()
        led_rouge.off ()
        led_vert.off ()
    elif LED_counter == 1:
        led_bleu.off ()
        led_vert.on ()
        led_rouge.off ()
    else:
        led_bleu.off ()
        led_vert.off ()
        led_rouge.on ()
    # We want to turn on the next LED at the next iteration of the loop
    led_counter = led_counter + 1
    if led_counter> 2:
        led_counter = 0
    time.sleep_ms (500) # the System falls asleep for 500ms
```
##Reading an analog value (ADC)

We would now like to convert the analog value (0-3.3V) of a signal to a digital value (0-4095).

You can connect to A0-5 an input returning a voltage between 0 and + 3.3V (possibly variable over time). This connector is plugged directly into the ADC of the microcontroller. The signal can therefore be converted into a digital signal:

![image](images/adc.png)


For this demonstration we will use a commercial potentiometer (10 KOhm) that we connect to A0, like this: 

![image](images/potentiometre.png)

Any 10 KOhm potentiometer reference will work for demonstration. Here is a reference:

[PTV09A-4020F-B103](https://www.mouser.fr/ProductDetail/Bourns/PTV09A-4020F-B103?qs=sGAEpiMZZMtC25l1F4XBU1xwXnrUt%2FuoeIXuGADl09o%3D)  : ![image](images/potentiometre2.png)

With the help of the following script, use the * pyb.ADC * function to interact with the STM32 ADC:



``` python
import pyb
import time
print ("ADC with MicroPython is easy")
# Initialization of the ADC on Pin A0
adc_A0 = pyb.ADC (pyb.Pin ('A0'))
while 1:
    numeric_value = adc_A0.read ()
    # It is now necessary to convert the numerical value compared to the reference voltage (3.3V) and
    # the number of converter bits (12 bits - 4096 values)
    analog_value = (numeric_value * 3.3) / 4095
    print ("The voltage value is:", analog_value, "V")
    # the System falls asleep for 500ms
    time.sleep_ms (500)
```

You can launch the Putty terminal and observe the value in Volts which evolves, every 500ms, when you turn the potentiometer.

## Display on an OLED LCD screen (I2C)

It is very easy to use an OLED display with MicroPython to display messages. We will see in this example how to connect the LCD screen in I2C, then how to control the screen to send messages with MicroPython.

For the example, we will use the monochrome OLED screen, 192 x 32 pixels, from Adafruit, however all screens integrating the SSD1306 driver are compatible.

Here's how to hook up the OLED display:
* D15 = SCL
* D14 = SDA

![image](images/oled.png)


We will need the file [ssd1306.py] (https://stm32python.gitlab.io/en/docs/Micropython/Download) available directly in the [TP LCD] (https://stm32python.gitlab.io/en/ docs / Micropython / Download)

When you have finished downloading, you will need to transfer the file to the PYBLASH device directory.

* Now edit the srcipt *** main.py ***:

```python
from machine import Pin, I2C
import ssd1306
from time import sleep

#Initialization of the I2C device
i2c = I2C (scl = Pin ('SCL'), sda = Pin ('SDA'), freq = 100000)

# Parameter setting of screen characteristics
oled_screen_width = 128
oled_screen_length = 32
oled = ssd1306.SSD1306_I2C (screen_width_oled, screen_length_oled, i2c)

#Send text to display on the OLED screen
oled.text ('MicroPython OLED!', 0, 0)
oled.text ('I2C', 0, 10)
oled.text ('Too easy !!!', 0, 20)
oled.show ()
```

## Using BLE (Bluetooth Low Energy)
In this part we will see how to communicate in Bluetooth Low Power with the STBLESensor application and the WB55 development board.

### Installing ST BLE Sensor on your smartphone

Install the ST BLE Sensor application on your smartphone on [Google Play](https://play.google.com/store/apps/details?id=com.st.bluems) ou [IOS Store](https://apps.apple.com/it/app/st-ble-sensor/id993670214)

![image](images/stmblesensorapp.png) ![Android](images/stmblesensorapp-qr-android.png)   ![iOS](images/stmblesensorapp-qr-ios.png)

Consult [the full description of the various services offered by the STBLESensor application](https://github.com/STMicroelectronics/STBlueMS_Android)<br>

### BLE communication in MicroPython
To communicate in Bluetooth Low Energy with micropython, you must include 2 new files in the directory of the usb disk * PYBFLASH *:
1. *** ble_advertising.py *** (Help file for creating the warning message)
2. *** ble_sensor.py *** (Class allowing the management of the BLE connection)
<br> You will need to download the necessary scripts for this example [TP BLE](https://stm32python.gitlab.io/en/docs/Micropython/Download

Thanks to the * ble_sensor.py * file, we will be able to create a BLE object with 1 service and 2 characteristics.

_This is the file that will need to be modified to change the BLE profile, if necessary._

After the script is started, the WB55 development kit begins to emit BLE frames, called * advertising *. These messages identify the Bluetooth object and signify that the device is ready to be connected.

The device name is: *** WB55-MPY ***. We will check with the smartphone application whether the WB55 card is in bluetooth transmission.

### Use
Launch the *** STBLESensor *** application on your SmartPhone:

![image](images/app1.png)


* Then press the magnifying glass icon to display the surrounding BLE devices:

![image](images/app2.png)

In this example, the BLE profile we have chosen allows us to simulate a thermometer and turn an LED on or off. The thermometer value is generated randomly every second.

* Connect to the development board by pressing “WB55-MPY”:

![image](images/app3.png)

The blue LED on the WB55 card should light up when it is connected to the application.
We can observe, on this screen, the random change in temperature between 0 and 100.0 ° C.
It is possible to display the temperature in graphic mode.

* To do this, press the menu button! [Image] (images / app-menu.png):

![image](images/app4.png)

*Now press![image](images/app-plot.png):

![image](images/app5.png)

*To display the graph, press![image](images/app-play.png):

![image](images/app6.png)

You can use the! [Image] button (images / app-options.png) to change chart options, such as the size of the X axis or enabling automatic Y scale change.

We are now going to study the sending of information from the SmartPhone to the WB55 platform.
For this we use the application to turn the red LED of the development kit on or off.

* For that, press the menu button![image](images/app-menu.png):

![image](images/app7.png)

* Now choose the option![image](images/app-switch.png):

![image](images/app8.png)

You can on this screen control the Red LED of the development kit.