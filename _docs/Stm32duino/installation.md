---
title: Stm32duino
description: How to install Stm32duino
---
# Installation of Stm32duino

## Installation of Arduino environment
Firstly it is necessary to use the Arduino IDE development environment.

Download and install Arduino IDE from the link [https://www.arduino.cc/en/Main/Software](https://www.arduino.cc/en/Main/Software)

### Install the STM32 package
Once Arduino IDE is installed, launch Arduino IDE then go to `File > Preferences`

A window opens :

![Image](exercises/images/1_installation/1.png)


In "Additional Boards Manager URLs", enter the following URL :
https://raw.githubusercontent.com/stm32duino/BoardManagerFiles/master/STM32/package_stm_index.json

Enter "OK"

Then : `Tools > Board : ___ > Boards Manager...`
![Image](exercises/images/1_installation/2.png)

Enter in the search bar "STM32" or "stm" and download the package by clicking on install.
![Image](exercises/images/1_installation/3.png)

### Download and install STM32CubeIDE

To be able to use your STM32 with Arduino environment, you have to install the STM32CubeIDE tool.
Download and install STM32CubeIDE available [here](https://www.st.com/en/development-tools/stm32cubeide.html).

> Warning : it is necessary to create a my.st.com account.

Launch Arduino IDE with a STM32 plugged in USB on the computer.

Go to `Tools > Board: ___` and choose `Nucleo-64` as shown in the picture below.
![Image](exercises/images/1_installation/5.png)

Then go to `Tools > Board part number: ___` and choose the desired STM32 board. Here the Nucleo WB55.
![Image](exercises/images/1_installation/6.png)

In `Tools > Upload method: ___` choose `STM32CubeProgrammer (SWD)`
![Image](exercises/images/1_installation/7.png)

Then : `Tools > Port: ___` and choose the corresponding port to your STM32 board.
![Image](exercises/images/1_installation/8.png)


You can now program your STM32.

Congratulation !
This tutorial is now completed.
