---
title: Exercises with Stm32duino in C/C++
description: Exercises with Stm32duino in C/C++

---
# Exercises with Stm32duino in C/C++

Here is the list of exercises with Stm32duino in C/C++ :

- [LED](del_blink) (translation in progress, currently availaible in french)
- [Grove sensors](grove) (translation in progress, currently availaible in french)
- [collision](choc) (translation in progress, currently availaible in french)
- [LED - Following](del) (translation in progress, currently availaible in french)
- [Brightness](luminosite) (translation in progress, currently availaible in french)
- [Potentiometer](potentiometre) (translation in progress, currently availaible in french)
- [Temperature](temperature) (translation in progress, currently availaible in french)
- [Joystick](joystick) (translation in progress, currently availaible in french)
- [Touch](toucher) (translation in progress, currently availaible in french)
- [Ultrasonic Distance](ultrason) (translation in progress, currently availaible in french)
- [Buzzer](buzzer) (translation in progress, currently availaible in french)
- [Noise](bruit) (translation in progress, currently availaible in french)
- [8x7-segments Display TM1638](tm1638)
- [Real Time Clock (RTC)](rtc)
- [MEMS expension card IKS01A1](iks01a1) (translation in progress, currently availaible in french)
- [MEMS expension card IKS01A3](iks01a3) (translation in progress, currently availaible in french)
- [GNSS X-NUCLEO-GNSS1A1 card](x-nucleo-gnss1a1) (translation in progress, currently availaible in french)
- [LoRa I-NUCLEO-LRWAN1 communication card](i-nucleo-lrwan1) (translation in progress, currently availaible in french)
- [Communication with Firmata](firmata) (translation in progress, currently availaible in french)
- [Piloting motors with the SparkFun Monster Moto Shield card](monster_moto) (translation in progress, currently availaible in french)
- [16 character x 2 lines LCD display](lcd_16x2) (soon)
- [UART GPS module SIM28](gps) (soon)
- [Nintendo Wii Nunchuk](nunchuk) (translation in progress, currently availaible in french)
- [Nintendo SNES Gamepad](snes) (translation in progress, currently availaible in french)
- [RFID badge](rfid) (translation in progress, currently availaible in french)
- [DS1820 waterproof temperature probe](ds1820) (soon)
