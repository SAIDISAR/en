---
title: Exercice avec Real Time Clock (RTC) en MicroPython
description: Exercice avec Real Time Clock (RTC) en MicroPython
---

# Real Time Clock

This tutorial explains how to implement the real time clock of the STM32WB55 with MicroPython.

The real-time clock (RTC) is an electronic circuit integrated into the STM32WB55 microcontroller fulfilling the functions of a very precise clock for time-stamping issues, management of alarms triggered according to a calendar and able to put the microcontroller to sleep or wake it up, etc. In order to be as precise and stable as possible, the RTC can use a 32kHz vibrating quartz as a frequency source.

## Usage
 
At first, it is necessary to include libraries :  
It can be downloaded from arduino IDE under `Sketch > Include Library > Manage libraries` and searching `STM32duino RTC`.
```c
// Importation des bibliothèques
#include <STM32RTC.h>
```  
  
Next we declare the RTC
```c
STM32RTC& rtc = STM32RTC::getInstance();
```  

Then we start it
```c
rtc.begin();
```  

We can now use the RTC. The main functions prototypes are as follows :
```c
void setHours(uint8_t hours);
void setMinutes(uint8_t minutes);
void setSeconds(uint8_t seconds);
void setTime(uint8_t hours, uint8_t minutes, uint8_t seconds, uint32_t subSeconds = 1000, AM_PM period = AM);

void setWeekDay(uint8_t weekDay);	// 1 for monday et 7 for sunday
void setDay(uint8_t day);
void setMonth(uint8_t month);
void setYear(uint8_t year);
void setDate(uint8_t weekDay, uint8_t day, uint8_t month, uint8_t year);

uint8_t getHours();
uint8_t getMinutes();
uint8_t getSeconds();
void getTime(uint8_t *hours, uint8_t *minutes, uint8_t *seconds, uint32_t *subSeconds, AM_PM *period = nullptr);

uint8_t getWeekDay();	// 1 for monday et 7 for sunday
uint8_t getDay();
uint8_t getMonth();
uint8_t getYear();
void getDate(uint8_t *weekDay, uint8_t *day, uint8_t *month, uint8_t *year);
```

For unwanted values of `getTime` and `getDate`, `NULL` can be specified for the call. e.g. `getTime(&hours, &minutes, &seconds, NULL);` allows to retrieve hours, minutes and seconds without subseconds.  

The date of Saturday, January 2, 2021 at 12:21 51s can the be defined as follows :
```c
rtc.setTime(12, 21, 51);
rtc.setDate(6, 2, 1, 21);
```  
and it can be retrieved like this : 
```c
uint8_t hours, minutes, seconds, weekDay, day, month, year;
rtc.getTime(&hours, &minutes, &seconds, NULL);
rtc.getDate(&weekDay, &day, &month, &year);
```  

## Examples  
  
So a first code to display on the terminal would be :
```c
// libraries include
#include <STM32RTC.h>

// we declare the RTC
STM32RTC& rtc = STM32RTC::getInstance();

void setup(){
	// we start the rtc
	rtc.begin();

	// we initialize date and time
	rtc.setTime(12, 21, 51);
	rtc.setDate(6, 2, 1, 21);
}

void loop() {
  uint8_t hours, minutes, seconds, weekDay, day, month, year;
  // we retrieve date and time
  rtc.getTime(&hours, &minutes, &seconds, NULL);
  rtc.getDate(&weekDay, &day, &month, &year);

  // we display
  printf("%d -- %d/%d/%d -- %02d:%02d:%02d\n", weekDay, day, month, year, hours, month, seconds);

  // every second
  delay(1000);
}
```  
  
With the help of the [8x7-segments TM1638 Display tutorial](https://stm32python.gitlab.io/en/docs/Stm32duino/exercises/tm1638) a clock with LED display can be made with the following example code :
```c
// libraries include
#include <STM32RTC.h>
#include <TM1638plus.h>

// we declare the RTC
STM32RTC& rtc = STM32RTC::getInstance();

// we declare the display's card
//           STB  CLK  DIO
TM1638plus tm(D2, D3 , D4);

void setup() {
  // we start the rtc
  rtc.begin();

  // we initialize date and time
  rtc.setTime(0, 0, 0);
  rtc.setDate(5, 1, 1, 21);

  // start of display
  tm.displayBegin();
  
  // we reduce the brightness
  tm.brightness(0);
}

void loop() {
  uint8_t hours, minutes, seconds, weekDay, day, month, year, buttons;
  char str[9];
  // we retrieve date and time
  rtc.getTime(&hours, &minutes, &seconds, NULL);

  if (seconds % 2){
    sprintf(str, "%02dh%02d .%02d", hours, minutes, seconds);
    tm.displayText(str);
  }
  else{
    sprintf(str, "%02dh%02d %02d", hours, minutes, seconds);
    tm.displayText(str);
  }
  // we retrieve information on the buttons to adjust time
  buttons = tm.readButtons();
    
  // we compare bit to bit to identify buttons
  if (buttons & 1) { 
    hours++;
    if (hours > 23) {
      hours = 0;
    }
    rtc.setHours(hours);
  }
  if (buttons & 1<<1) {
    minutes++;
    if (minutes > 59) {
      minutes = 0;
    }
    rtc.setMinutes(minutes);
  }
  if (buttons & 1<<2) {
    seconds++;
    if (seconds > 59) {
      seconds = 0;
    }
    rtc.setSeconds(seconds);
  }
    
  // we refresh every 100 ms (for a more sensitive read of the buttons it can be changed)
  delay(100);
}
```
