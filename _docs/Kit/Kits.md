---
title: Other STM32Python kits
description: Description of other STM32Python educational kits
categories: stm32 kit nucleo python
---

# The LoRaWAN Kit

The boards included in educational kit 2 are:

* [NUCLEO-F446RE - Development board, MCU STM32F446RE Debugger on board, Arduino, ST Zio and Morpho compatible](https://www.st.com/en/evaluation-tools/nucleo-f446re.html) (en sale at [Farnell](https://fr.farnell.com/stmicroelectronics/nucleo-f446re/carte-de-dev-arduino-mbed-nucleo/dp/2491978)).

* [I-NUCLEO-LRWAN1 - Expansion Board, USI LoRa LPWAN Module, Multiple Sensors for Nucleo STM32, Arduino Compatible](https://www.st.com/en/evaluation-tools/i-nucleo-lrwan1.html) (available from [Farnell](https://fr.farnell.com/stmicroelectronics/i-nucleo-lrwan1/carte-d-extension-mcu-arm-cortex/dp/2809319)).

* [Grove connectors board](http://wiki.seeedstudio.com/Grove_System/#grove-starter-kit) for connecting [Grove](http://wiki.seeedstudio.com/Grove_System/) sensors and extra sensors (potentiometer , ultrasound, thermometer, LED display, infrared receivers, LCD display ...) (on sale at [Farnell](https://fr.farnell.com/seeed-studio/83-16991/grove-starter-kit-for-arduino/dp/2801858?st=Grove) and at [Seeedstudio](https://www.seeedstudio.com/Base-Shield-V2.html)). _This card is recommended but it is not mandatory._

* [P-Nucleo-LRWAN2 : STM32 Nucleo pack LoRa™ HF band sensor and gateway](https://www.st.com/en/evaluation-tools/p-nucleo-lrwan2.html). _This kit is recommended but it is not mandatory._ (on sale at [Farnell](https://fr.farnell.com/stmicroelectronics/p-nucleo-lrwan2/carte-d-eval-application-bande/dp/3262489?st=LRWAN))

* [Gateway LoRaWAN TTIG](https://www.thethingsnetwork.org/docs/gateways/thethingsindoor/) (on sale at [RS-Online](https://fr.rs-online.com/web/p/radio-frequency-development-kits/1843981/))

| | | |
| - | - | - |
![nucleo-f446re](images/nucleo-f446re.jpg) | ![i-nucleo-lrwan1](images/i-nucleo-lrwan1.jpg) | ![grove_starter_kit](images/grove_starter_kit.jpg)


# Other STM32 kits for the future

* [STEVAL-STLKT01V1 - Development Kit, SensorTile IoT Module, MCU STM32L476JGY, Miniature Square Shape](https://www.st.com/en/evaluation-tools/steval-stlkt01v1.html) (on sale at [Farnell ](https://fr.farnell.com/stmicroelectronics/steval-stlkt01v1/carte-de-developpement-capteur/dp/2664520))

* [STEVAL-BCN002V1B BlueTile - Bluetooth LE enabled sensor node development kit](https://www.st.com/en/evaluation-tools/steval-bcn002v1b.html) (on sale at [Farnell](https://fr.farnell.com/stmicroelectronics/steval-bcn002v1b/kit-d-eval-bluetooth-low-energy/dp/3009966)).

* [B-L072Z-LRWAN1 - Discovery Kit, LoRa® Low Power Wireless Module, SMA and U.FL RF Interface Connectors](https://www.st.com/en/evaluation-tools/b-l072z-lrwan1.html) (available from [Farnell](https://fr.farnell.com/stmicroelectronics/b-l072z-lrwan1/kit-discovery-iot-connectivity/dp/2708776))

* [B-L475E-IOT01A Discovery kit for IoT node](https://www.st.com/en/evaluation-tools/b-l475e-iot01a.html) (on sale at [Farnell](https://fr.farnell.com/stmicroelectronics/b-l475e-iot01a2/kit-discovery-iot-node-868mhz/dp/2708778))

* [X-NUCLEO-GNSS1A1 GNSS expansion board](https://www.st.com/en/ecosystems/x-nucleo-gnss1a1.html) (available from [Farnell](https://fr.farnell.com/stmicroelectronics/x-nucleo-gnss1a1/card-extension-gnss-card-nucleo/dp/2980979?ost=X-NUCLEO-GNSS1A1)


* [P-NUCLEO-IHM03 motor control kit](https://www.st.com/en/evaluation-tools/p-nucleo-ihm03.html)

* [EVALKIT-ROBOT-1 evaluation kit](https://www.st.com/en/embedded-software/stsw-robot-1.html)

| | | |
| - | - | - |
![steva-stlkt01v1](images/steva-stlkt01v1.jpg) | ![steval-bcn002v1b](images/steval-bcn002v1b.jpg) | ![b-l072z-lrwan1](images/b-l072z-lrwan1.jpg)
![b-l475e-iot01a](images/b-l475e-iot01a.jpg) | ![x-nucleo-gnss1a1](images/x-nucleo-gnss1a1.jpg) | |


Image credits:
* [ST Microelectronics](https://www.st.com)
* [Seeedstudio](http://wiki.seeedstudio.com/Grove_System)